#encoding: utf-8
require 'open-uri'

gem 'bootstrap-sass', '~> 3.3.6'
gem 'font-awesome-sass', '~> 4.5.0'
gem "breadcrumbs_on_rails"
gem 'haml-rails'
gem 'bootstrap_form'
gem 'jquery-ui-rails'
gem 'flexa_downloadable'
gem 'mina'

application do
  "config.generators do |g|
    g.orm             :active_record
    g.template_engine :haml
    g.test_framework  :test_unit, fixture: false
    g.stylesheets     false
    g.javascripts     false
  end"
end

after_bundle do
  git :init
  git add: "."
  git commit: %Q{ -m 'Initial commit' }
end

run <<-SHELL
 git clone git@bitbucket.org:gilgomes/flexa-template.git
 mv flexa-template/template/app/assets/images/* app/assets/images/
 mv flexa-template/template/app/assets/javascripts/* app/assets/javascripts/
 mv flexa-template/template/app/assets/stylesheets/* app/assets/stylesheets/
 mv flexa-template/template/app/views/layouts/* app/views/layouts/
 mv flexa-template/template/app/views/_* app/views/
 mv flexa-template/template/lib/templates/ lib/
 mv flexa-template/template/public/* public/
 rm -rf app/helpers/application_helper.rb
 mv flexa-template/template/app/helpers/application_helper.rb app/helpers/
 mkdir -p lib/active_record_extension
 mv flexa-template/template/lib/active_record_extension/* lib/active_record_extension
 mv flexa-template/template/config/initializers/* config/initializers/
 rm -rf flexa-template
SHELL

run "rm app/assets/stylesheets/application.css"
file 'app/assets/stylesheets/application.scss', <<-CODE
  /*
 *= require_tree .
 *= require_self
 */

@import "font-awesome-sprockets";
@import "font-awesome";
@import "bootstrap-sprockets";
@import "bootstrap";
CODE


file "app/assets/stylesheets/cloud-admin-template/default.css", open("https://bitbucket.org/!api/2.0/snippets/gilgomes/6bjyy/56b1e305a60d8325e80725d267620d452d7e81b9/files/default.css").read
file "app/assets/stylesheets/cloud-admin-template/responsive.css", open("https://bitbucket.org/!api/2.0/snippets/gilgomes/6bjyy/56b1e305a60d8325e80725d267620d452d7e81b9/files/responsive.css").read
file "app/assets/stylesheets/cloud-admin-template/cloud-admin.css", open("https://bitbucket.org/!api/2.0/snippets/gilgomes/6bjyy/56b1e305a60d8325e80725d267620d452d7e81b9/files/cloud-admin.css").read
