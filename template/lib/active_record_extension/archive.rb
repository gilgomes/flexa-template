module Archive
  extend ActiveSupport::Concern

  def destroy
    if respond_to?(:archived?)
      archive
    else
      super
    end
  end



  def archive
    update_attribute(:archived, true) if respond_to? :archived
  end

  module ClassMethods
    def archiveds
      unscoped.where(archived: true) if attribute_names.include? "archived"
    end

    def default_scope
      return where(archived: false) if attribute_names.include? 'archived'
      super
    end
  end
end

ActiveRecord::Base.send(:include, Archive)
