module ApplicationHelper
  def render_page_header(args)
    html = <<-HTML
    <div class="row">
      <div class="col-sm-12">
        <div class="page-header">
          <ul class="breadcrumb">
             #{render_breadcrumbs separator: '', tag: :li}
          </ul>
          <div class="clearfix">
            <h3 class="content-title pull-left">#{args[:title]}</h3>
          </div>
          <div class="description">#{args[:description]}</div>
        </div>
      </div>
    </div>
    HTML
    html.html_safe
  end

  def opacity klass
    if klass.respond_to?(:archived) && klass.archived?
      "opacity: 0.5;"
    else
      ""
    end
  end
end
